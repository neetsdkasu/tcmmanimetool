TCMM Anime Tool
===============

TCMMのテスターでアニメ生成するのをサポートするツール(雑)(予定)



絵を`BufferedImage`に一度描いてからアニメ表示してるタイプのテスターで使える(今のとこ)  



## 今後の課題(予定？)  

 - javadocくらいは書こうぜ  
 - この`Anime`自体で`BufferedImage`クラスを取り扱ってフレームを直接描画できるようにしたい  
 - テスターとソリューションの標準入出力のやりとりでアニメ描画のやりとり処理など追加できるかもしれない  
 - アニメだけじゃなく静止画も生成できるといいかも  
 - フレームごとのスピード調整の方法を調べて調整できるようにしたいね  

 
## 使い方(雑)  

テスターのソースコードを `FooVis.java`  
テスターのjarは `tester.jar`  
解法の実行ファイルを `Foo.exe`  
として説明すると  


### ファイル

`Anime.jar`をテスターのソースと同じディレクトリに置く  
(`Anime.jar`のダウンロードは[Tagページ](https://gitlab.com/neetsdkasu/tcmmanimetool/tags)にて)  



`Anime.jar`じゃなくソースファイル直接使う場合は  
テスターのソースのディレクトリに`mmtools`というディレクトリを作って  
その`mmtools`下に`Anime.java`, `AnimeFile.java`を配置する  



※これらのファイルを別のディレクトリ(例えば`lib`など)に配置したい場合はコンパイルや実行時のパス指定にそのディレクトリパスの修正  
※例えば`lib`下に置く場合は`lib\Anime.jar`とか`lib\mmtools\Anime.java`,`lib\mmtools\AnimeFile.java`とかになる  


### テスターソースの編集  


テスターのソースファイル冒頭に  
```java
import mmtools.*;  
```
の一行を追加  



テスターのエントリーポイントである`main`メソッド内にて  
`-anime`フラグでも適当に拾って  
```java
Anime.setEnable(true);  
vis = ture;  
```
とか書いておく  
(`vis`はテスターソースに元々あるビジュアライザ有効化のフラグ)  
この`setEnable`を`true`にしないと以降の`Anime`のメソッド内処理は無視される  



テスターのテスト実行メソッド(たいてい`runTest(seed)`メソッド)の冒頭あたりに  
```java
Anime.init(seed + ".gif");  
```
とか書いておく  
保存するファイル名を決定する  



テスターの描画メソッド(たとえば`paint(g)`メソッドとか)の描画終了してるあたりに  
このメソッドの冒頭くらいに`BufferedImage bi = new BufferedImage(～ `みたいなコードあるので  
このメソッドの終わりあたりに  
```java
Anime.addFrame(bi);  
```
とか書いておく
これはアニメのフレームとして追加する  
※注意として`paint(g)`はGUIの仕組み的に呼び出しタイミングがプログラマからは予測不能なためコマ落ちしたり同じコマが描画される場合などあるので実はgifアニメ生成にあまり適してない  



テスターの終了処理のあたり(`"Score = "`みたいな最終スコア表示する処理のあたり)のところに  
```java
Anime.close();  
```
とか書いておく  
必要な終了処理  



### コンパイル

テスターコンパイル時にクラスパスに`Anime.jar`を指定してコンパイルする  
```
javac -cp Anime.jar FooVis.jar  
```


コンパイルした`FooVis`を`tester.jar`に含めるなら  
```
jar ufve tester.jar FooVis -C . *.class
```



`Anime.jar`ではなくソースを使ってコンパイルする場合は   
ソースディレクトリ下の`mmtools`に`Anime.java`等があるなら  
```
javac Foovis.jar
```
でコンパイルできる  
`tester.jar`に含める場合は  
```
jar ufve tester.jar FooVis -C . *.class
jar ufv tester.jar mmtools
```
となる  



`lib`下にファイル配置してるならそれぞれ  
```
javac -cp lib\Anime.jar FooVis.java
```
```
javac -sourcepath lib FooVis.java

jar ufve tester.jar FooVis -C . *.class
jar ufv tester.jar -C lib mmtools
```
となる  


### 実行

`tester.jar`に含めてない場合は  
```
java -cp Anime.jar;. FooVis -seed 123 -exec "Foo.exe"  
```


`tester.jar`に含めている場合は(`-jar`フラグで実行できないらしい)  
```
java -cp Anime.jar;tester.jar FooVis -seed 123 -exec "Foo.exe"  
```


※`tester.jar`内のマニフェストファイルのクラスパスの指定に`Anime.jar`の名前を追加しておけば`-jar`フラグで実行できるらしい(少し手間)  




`Anime.jar`を使わず直接ソース(`Anime.java`等) を使った場合は  
```
java -cp mmtools;. FooVis -seed 123 -exec "Foo.exe"
```
`tester.jar`に含めた場合は通常どおりでOK   
```
java -jar tester.jar -seed 123 -exec "Foo.exe"
```



`lib`下に配置した場合は  
`Anime.jar`の場合はそれぞれ  
```
java -cp lib\Anime.jar;. FooVis -seed 123 -exec "Foo.exe"  
```
```
java -cp lib\Anime.jar;tester.jar FooVis -seed 123 -exec "Foo.exe"  
```
ソースの場合で`tester.jar`を使用しない場合は  
```
java -cp lib;. FooVis -seed 123 -exec "Foo.exe"
```


### ソース編集の例

[MM98](http://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=17086&pm=14823) の [PrincessesAndMonstersVis.java](https://www.topcoder.com/contest/problem/PrincessesAndMonsters/manual.html) に追加した例  


ソース編集例(Gistでのdiff) https://git.io/fx2oN

```diff
--- PrincessesAndMonstersVis.java	2018-10-18 01:14:03.780975700 +0900
+++ PrincessesAndMonstersVis2.java	2018-10-17 22:59:31.378230400 +0900
@@ -6,6 +6,8 @@
 import java.util.*;
 import javax.swing.*;
 
+import mmtools.*;
+
 // --------------------------------------------------------
 class State {
     public int r, c;    // row and column of the room in the dungeon
@@ -71,6 +73,8 @@
         rnd = SecureRandom.getInstance("SHA1PRNG");
         long seed = Long.parseLong(seedStr);
         rnd.setSeed(seed);
+        
+        Anime.init(seed + ".gif");
 
         S = rnd.nextInt(maxS - minS + 1) + minS;
         K = rnd.nextInt(maxK - minK + 1) + minK;
@@ -577,6 +581,7 @@
             }
 
             g.drawImage(bi,0,0, SZW, SZH, null);
+            Anime.addFrame(bi);
         }
         // -------------------------------------
         public Vis() {
@@ -627,7 +632,7 @@
             catch (Exception e) { e.printStackTrace(); }
 
         System.out.println("Score = " + s);
-
+        Anime.close();
         if (proc != null)
             try { proc.destroy(); }
             catch (Exception e) { e.printStackTrace(); }
@@ -654,6 +659,8 @@
                 SZ = Integer.parseInt(args[++i]);
             if (args[i].equals("-timelimit"))
                 TL = Integer.parseInt(args[++i]);
+            if (args[i].equals("-anime"))
+                Anime.setEnable(vis = true);
         }
         PrincessesAndMonstersVis f = new PrincessesAndMonstersVis(seed);
     }
```
