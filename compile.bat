@pushd "%~dp0"
@setlocal

@set classdir=classes
@set srcdir=src
@set bindir=bin

@set srcfile=%srcdir%\mmtools\Anime.java
@set jarpath=%bindir%\Anime.jar

@if not exist "%classdir%" ( mkdir "%classdir%" )

javac ^
    -encoding "utf8" ^
    -d "%classdir%" ^
    -sourcepath "%srcdir%" ^
    "%srcfile%"

@if ERRORLEVEL 1 ( goto endlabel )

@if not exist "%bindir%" ( mkdir "%bindir%" )

@if exist "%jarpath%" ( goto updatejar ) else ( goto makejar )


:makejar

jar cvf "%jarpath%" -C "%classdir%" .

@goto endlabel


:updatejar

jar uvf "%jarpath%" -C "%classdir%" .

@goto endlabel


:endlabel

@endlocal
@popd
