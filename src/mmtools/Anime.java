package mmtools;

import java.awt.image.RenderedImage;
import java.io.File;

public final class Anime
{
    private Anime() {}

    private static AnimeFile animeFile = null;

    private static boolean enable = false;

    public static void setEnable(boolean enable)
    {
        Anime.enable = enable;
    }

    public static void init(File file)
    {
        close();
        if (enable)
        {
            animeFile = new AnimeFile(file);
        }
    }

    public static void init(String pathname)
    {
        close();
        if (enable)
        {
            animeFile = new AnimeFile(pathname);
        }
    }

    public static void addFrame(RenderedImage img)
    {
        if (!enable || animeFile == null) { return; }
        animeFile.addFrame(img);
    }

    public static void close()
    {
        if (animeFile == null) { return; }
        try
        {
            animeFile.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        animeFile = null;
    }
}