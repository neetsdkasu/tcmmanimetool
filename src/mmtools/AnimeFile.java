package mmtools;

import java.awt.image.RenderedImage;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

public class AnimeFile implements Closeable
{
    private static Logger getLogger()
    {
        return Logger.getLogger("mmtools");
    }

    ImageWriter imgWriter = null;
    ImageOutputStream imgOutStrem = null;

    File file = null;
    boolean initialized = false;
    boolean failed = false;
    boolean released = false;

    public AnimeFile() {}

    public AnimeFile(File file)
    {
        setFile(file);
    }

    public AnimeFile(String pathname)
    {
        setFile(pathname);
    }

    public AnimeFile setFile(File file)
    {
        if (initialized || failed || released) { return this; }

        if (file == null)
        {
            getLogger().warning("file is null");
        }

        this.file = file;

        return this;
    }

    public AnimeFile setFile(String pathname)
    {
        if (initialized || failed || released) { return this; }

        if (pathname == null)
        {
            getLogger().warning("pathname is null");
            this.file = null;
            return this;
        }

        pathname = pathname.trim();

        if (pathname.length() == 0)
        {
            getLogger().warning("pathname is wrong");
            this.file = null;
            return this;
        }

        if (!pathname.toLowerCase().endsWith(".gif"))
        {
            pathname += ".gif";
        }

        try
        {
            this.file = new File(pathname);
        }
        catch (Exception ex)
        {
            getLogger().throwing("AnimeFile", "setFile(String)", ex);
            this.file = null;
        }

        return this;
    }

    @Override
    protected void finalize()
    {
        release();
    }

    public void close() throws IOException
    {
        release();
    }

    public void dispose()
    {
        release();
    }

    public void release()
    {
        if (released) { return; }

        if (initialized && imgWriter != null && imgOutStrem != null)
        {
            try
            {
                imgWriter.endWriteSequence();
            }
            catch (Exception ex)
            {
                getLogger().throwing("AnimeFile", "release", ex);
            }
        }

        if (imgWriter != null)
        {
            imgWriter.dispose();
            imgWriter = null;
        }

        if (imgOutStrem != null)
        {
            try
            {
                imgOutStrem.close();
            }
            catch (Exception ex)
            {
                getLogger().throwing("AnimeFile", "release", ex);
            }
            imgOutStrem = null;
        }

        initialized = false;
        released = true;
    }

    boolean init()
    {
        if (released || failed) { return false; }
        if (initialized) { return true; }

        if (file == null)
        {
            failed = true;
            getLogger().finer("file is null");
            return false;
        }

        try
        {
            Iterator<ImageWriter> iter = ImageIO.getImageWritersBySuffix("gif");
            if (!iter.hasNext())
            {
                failed = true;
                getLogger().finer("no ImageWriter for gif");
                return false;
            }
            imgWriter = iter.next();
        }
        catch (Exception ex)
        {
            failed = true;
            getLogger().throwing("AnimeFile", "init", ex);
            return false;
        }

        try
        {
            imgOutStrem = new FileImageOutputStream(file);
        }
        catch (Exception ex)
        {
            failed = true;
            getLogger().throwing("AnimeFile", "init", ex);
            release();
            return false;
        }

        try
        {
            imgWriter.setOutput(imgOutStrem);
        }
        catch (Exception ex)
        {
            failed = true;
            getLogger().throwing("AnimeFile", "init", ex);
            release();
            return false;
        }

        try
        {
            imgWriter.prepareWriteSequence(null);
        }
        catch (Exception ex)
        {
            failed = true;
            getLogger().throwing("AnimeFile", "init", ex);
            release();
            return false;
        }

        initialized = true;

        return true;
    }

    public AnimeFile addFrame(RenderedImage img)
    {
        if (img == null)
        {
            getLogger().finer("img is null");
            return this;
        }

        if (!init())
        {
            getLogger().finer("failed");
            return this;
        }

        try
        {
            imgWriter.writeToSequence(new IIOImage(img, null, null), null);
        }
        catch (Exception ex)
        {
            failed = true;
            getLogger().throwing("AnimeFile", "addFrame", ex);
            release();
        }

        return this;
    }

}