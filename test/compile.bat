@pushd "%~dp0"
@if /I "%~1"=="all" ( call ..\compile.bat )
@if ERRORLEVEL 1 ( goto endlabel )
@if not exist classes ( mkdir classes )

javac -cp ..\bin\Anime.jar -d classes -sourcepath src src\Main.java

:endlabel
@popd