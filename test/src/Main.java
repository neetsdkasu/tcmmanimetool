import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import javax.imageio.*;
import javax.imageio.stream.*;

import mmtools.*;

class Main
{
    public static void main(String[] args) throws Exception
    {
        (new Main()).doIt(args);
    }

    void doIt(String[] args) throws Exception
    {
        testAnime();
    }

    void testAnime()
    {
        System.out.println("testAnime");

        Anime.setEnable(true);

        Anime.init("bar.gif");

        Anime.addFrame(makeImage(0));
        Anime.addFrame(makeImage(1));
        Anime.addFrame(makeImage(2));
        Anime.addFrame(makeImage(1));

        Anime.close();
    }

    void test() throws Exception
    {
        System.out.println("test");
        ImageWriter iw = getGifWriter();
        if (iw == null)
        {
            System.out.println("no gif writer");
            return;
        }
        System.out.println(iw);

        ImageWriteParam iwp = iw.getDefaultWriteParam();
        System.out.println(iwp);

        File file = new File("foo.gif");
        ImageOutputStream ios = new FileImageOutputStream(file);

        try
        {
            iw.setOutput(ios);
            iw.prepareWriteSequence(null);

            for (int i = 0; i < 3; i++) writeImage(i, iw);

            iw.endWriteSequence();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        iw.dispose();

        ios.close();
    }

    ImageWriter getGifWriter() throws Exception
    {
        Iterator<ImageWriter> iter = ImageIO.getImageWritersBySuffix("gif");
        if (!iter.hasNext())
        {
            return null;
        }
        return iter.next();
    }

    void writeImage(int flag, ImageWriter iw) throws Exception
    {
        BufferedImage img = makeImage(flag);

        // iw.write(img);
        iw.writeToSequence(new IIOImage(img, null, null), null);
    }

    BufferedImage makeImage(int flag)
    {
        try
        {
            flag = Math.abs(flag) % 3;

            int W = 120, H = 120;

            BufferedImage img = new BufferedImage(W, H, BufferedImage.TYPE_INT_BGR);
            Graphics2D g = img.createGraphics();

            g.setColor(Color.BLACK);
            g.fillRect(0, 0, W, H);

            int size = W / 7;
            int ch = (H - size) / 2;

            g.setColor(Color.LIGHT_GRAY);
            g.fillOval(0, (H - size * 2) / 2, W, size * 2);


            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                case 0: g.setColor(Color.BLUE); break;
                case 1: g.setColor(Color.YELLOW); break;
                case 2: g.setColor(Color.RED); break;
                }
                int x = size * (i * 2 + 1);
                if (i == flag)
                {
                    g.fillOval(x, ch, size, size);
                }
                else
                {
                    g.drawOval(x, ch, size, size);
                }
            }

            g.dispose();

            return img;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}







